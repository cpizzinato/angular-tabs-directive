'use strict';

angular.module('common.ui.tabs.controllers', []).controller('TabsCtrl', ['$rootScope', '$scope', '$location', '$route',
    function ($rootScope, $scope, $location, $route) {
    var ctrl = this;
    var tabs = $scope.tabs = [];

    /**
     * @method skipReload
     * @desc Binds a locationChangeSuccess listener so that the route
     * locals are reassigned and not refetched when going between tabs.
     * @return $location
     */
    function skipReload () {
        var lastRoute = $route.current;
        var unbind = $scope.$on('$locationChangeSuccess', function () {
             if (!$route.current.locals) {
                $route.current.locals = lastRoute.locals;
             }
            unbind();
        });
        return $location;
    }

    /**
     * @method select
     * @desc Clears the current active tab and updates the selected
     * tabs status to active.
     *
     * If the tab set has the attribute of change-path, the route on which
     * the tabs are defined assumes the route path to have a routeParam
     * ":subView?" that indicates the current tab view selected.
     * @param {Object} selectedTab - tab to become active.
     */
    ctrl.select = function (selectedTab) {
        angular.forEach(tabs, function (tab) {
            if (tab.active && tab !== selectedTab) {
                tab.active = false;
                tab.onDeselect();
            }
        });

        selectedTab.active = true;
        $scope.viewState = selectedTab.viewPath;
        $scope.isOpen = false;

        if ($scope.changePath) {

            // If the subView has not changed let angular handle the route change
            if ($route.current.pathParams.subView === selectedTab.routeKey) {
                return;
            }

            $route.current.pathParams.subView = selectedTab.routeKey;

            if (selectedTab.route) {
                skipReload().path(selectedTab.route);
            } else {

                // Determines if the user default tab is open.
                var result = selectedTab.routeKey ? $route.current.originalPath.replace('?', '') :
                    $route.current.originalPath.replace('\/:subView\?', '');

                for (var param in $route.current.pathParams) {
                    var regex = new RegExp(':' + param + '\\*?');
                     result = result.replace(regex, $route.current.pathParams[param]);
                }
            }
            skipReload().path(result);
        }

        // Run any additional operations definied on the scope of the tabs
        selectedTab.onSelect();
    };

    /**
     * @method goto
     * @desc Jump to a particular tab within the current tab set.
     * @param {string} state(tab) In which you want to switch too
     */
    ctrl.goto = function (state) {
        var selectedTab;
        angular.forEach(tabs, function(tab) {
            if (!tab.active && (tab.routeKey === state || tab.title === state)) {
                selectedTab = tab;
                ctrl.select(selectedTab);
                return;
            }
        });

    };

    /**
     * @method addTab
     * @desc Adds a tab to the current tab set. If there is only
     * 1 tab set it as active, else if the subView is specified
     * in path set that tab to active on adding to tab set.
     */
    ctrl.addTab = function (tab) {
        tabs.push(tab);
        if (tabs.length === 1) {
            tab.active = true;
            $scope.viewState = tab.viewPath;
            $scope.isOpen = false;
            tab.onSelect();
        }  else if ($scope.changePath &&
                $route.current.params.subView === tab.routeKey) {
            ctrl.select(tab);
        }
    };

    /**
     * @method removeTab
     * @desc Finds the tab to be removed in the tab set and removes
     * the tab from the array,
     */
    ctrl.removeTab = function (tab) {
        var index = tabs.indexOf(tab);
        tabs.splice(index, 1);
    };

}]);
