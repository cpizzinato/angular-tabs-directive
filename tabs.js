'use strict';

angular.module('common.ui.tabs', [
	'common.ui.tabs.controllers',
	'common.ui.tabs.directives'
]);