'use strict';

angular.module('common.ui.tabs.directives', [])
.directive('tabs', [function () {
    return {
        restrict: 'AE',
        transclude: 'true',
        replace: true,
        templateUrl: '/partials/tabs.html',
        controller: 'TabsCtrl',
        link: {
            pre: function (scope, element, attrs) {
                scope.changePath = angular.isDefined(attrs.changePath);
                scope.class= angular.isDefined(attrs.class) ? attrs.class: '';
                scope.isOpen = false;
                scope.optionsTitle = angular.isDefined(attrs.optionsTitle) ? attrs.optionsTitle: 'common.select_page';
            },
            post: function (scope, element, attrs, tabsCtrl) {
                scope.goto = function (state) {
                    tabsCtrl.goto(state);
                }
            }
        }
    };
}])

.directive('tab', [function () {
    return {
        require: '^tabs',
        restrict: 'AE',
        replace: true,
        templateUrl: function(tElement, tAttrs) {
            var type = tAttrs.type ? '_' + tAttrs.type : '';
            return '/partials/tab' + type + '.html';
        },
        scope: {
            active: '=?',
            enabled: '=?',
            title: '@',
            icon: '@',
            viewPath: '@',
            routeKey: '@',
            route: '@',
            onSelect: '&select',
            onDeselect: '&deselect'
        },
        link: function (scope, element, attrs, tabsCtrl) {

            // Check if tab is disabled in set
            scope.disabled = false;
            if (attrs.disabled) {
                  scope.$parent.$watch($parse(attrs.disabled), function(value) {
                    scope.disabled = !!value;
                  });
            }

            scope.select = function () {
                if (!scope.disabled) {
                    scope.active = true;
                    tabsCtrl.select(scope);
                }
            };


            if (scope.enabled || scope.enabled === undefined) {
                tabsCtrl.addTab(scope);
            }
            else {
                element.remove();
            }

            // Clean up when tabs are destroyed
            scope.$on('$destroy', function () {
                tabsCtrl.removeTab(scope);
            });
        }
    };
}]);
